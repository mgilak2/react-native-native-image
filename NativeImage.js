import React, { Component, PropTypes } from 'react';
import { NativeModules, Image, Text } from 'react-native';

export default class NativeImage extends Component {
  state = {
    base64: ""
  };

  componentDidMount() {
    NativeModules.Base64.convert(this.props.source.uri).then(
      base64 => {
        this.setState({base64});
      },
      error => console.log('error log', {...error})
    )
  }

  base64Format(uri, base64) {
    if (uri.includes(".jpg")) {
      return `data:image/jpeg;base64,${base64}`;
    } else if (uri.includes(".png")) {
      return `data:image/png;base64,${base64}`;
    }
  }

  render() {
    let {base64} = this.state;
    let {uri} = this.props.source;

    if (base64.length !== 0)
      return (<Image source={{...this.props.source, uri: `${this.base64Format(uri, base64)}`, isStatic: true}}/>);
    else
      return (<Text> </Text>);
  };
}

