#React Native Native Image
I was doing well with react native till I found out that I cant simply require an image
from sdcard or phone storage (anything outside of application directory). so I created this
to help me about it.

 
###NativeImage.js

```js
import React, { Component, PropTypes } from 'react';
import { NativeModules, Image, Text } from 'react-native';

export default class NativeImage extends Component {
  state = {
    base64: ""
  };

  componentDidMount() {
    NativeModules.Base64.convert(this.props.source.uri).then(
      base64 => {
        this.setState({base64});
      },
      error => console.log('error log', {...error})
    )
  }

  base64Format(uri, base64) {
    if (uri.includes(".jpg")) {
      return `data:image/jpeg;base64,${base64}`;
    } else if (uri.includes(".png")) {
      return `data:image/png;base64,${base64}`;
    }
  }

  render() {
    let {base64} = this.state;
    let {uri} = this.props.source;

    if (base64.length !== 0)
      return (<Image source={{...this.props.source, uri: `${this.base64Format(uri, base64)}`, isStatic: true}}/>);
    else
      return (<Text> </Text>);
  };
}

```

###MainActivity.java

```java
package me.gilak.sample;

import android.content.Intent;


import me.gilak.Base64ImageConverter.Base64Package;

import com.facebook.react.ReactActivity;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends ReactActivity {
    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    @Override
    protected String getJSBundleFile() {
        return CodePush.getBundleUrl();
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "EvandDiscovery";
    }

    /**
     * Returns whether dev mode should be enabled.
     * This enables e.g. the dev menu.
     */
    @Override
    protected boolean getUseDeveloperSupport() {
        return BuildConfig.DEBUG;
    }

    /**
     * A list of packages used by the app. If the app uses additional views
     * or modules besides the default ones, add more packages here.
     */
    @Override
    protected List<ReactPackage> getPackages() {
        return Arrays.<ReactPackage>asList(
                new Base64Package()
        );
    }
}


```



#####Any contributions are welcome