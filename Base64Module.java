package me.gilak.Base64ImageConverter;

import android.util.Base64;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


public class Base64Module extends ReactContextBaseJavaModule {

    public Base64Module(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "Base64";
    }

    @ReactMethod
    public void convert(String filePath, final Promise promise) {
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(filePath);
            byte[] bytes;
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }

                bytes = output.toByteArray();
                String encodedImage = Base64.encodeToString(bytes, Base64.DEFAULT);

                promise.resolve(encodedImage);
            } catch (IOException e) {
                e.printStackTrace();
                promise.reject("failed", "what ! : " + e.getMessage());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            promise.reject(
                    "there is no such a file : "
                            + filePath
                            + " this is the log : "
                            + e.getMessage(),
                    "failed");
        }

    }
}
